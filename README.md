# KeePass Plugin to unmount VeraCrypt Volume when all KeePass DB are locked #


### Description ###

Plugin to automatically dismount selected or all VeraCrypt mounted volumes when all KeePass databases are locked/closed or when KeePass is exited. The plugin dismounts VeraCrypt volumes after a file is locked/closed but makes sure it's the last file that was unlocked/opened.

It's a fork of [TrueCrypt AutoDismount for KeePass2](https://sourceforge.net/projects/tcad-kp2/) 

It's working as well a the previous plugin for TrueCrypt

### current version: 1.0.3.0 ###
Issue/Request implemented
 
 fix looking for executable with file dialog in Linux
 
### Older release notes ###

### version: 1.0.2.0 ###
Issue/Request implemented

 support Ubuntu/Mint PPA Build :
 
    - ubuntu 17.10 with keepass2 from ubuntu universe repo.

    - mint 18.2 with ppa 

    Windows disk letter are remapped to VeraCrypt slot number.
	
#### version: 1.0.1.0 ####

Issue/Request implemented

 #1: Request: Support the use of VeraCrypt portable; Added VeraCrypt Path setting in Option panel, to accommodate VeraCrypt Portable users (non installed version, no info in registry)

*Note: You will need to redo the plugin configuration after this install, as the config structure changed*

### Downloads ###

[Download page](https://bitbucket.org/fbourqui/veracryptautodismount/downloads/)

## Installation ##

Copy and unzip one of the download into your KeePass plugin directory.