﻿/*
  Plugin for KeePass Password Safe - The Open-Source Password Manager
  VeraCryptAutoDismount
  Copyright (C) 2015-2017 Frédéric Bourqui

  fork of:
  TrueCryptAutoDismountGT

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using KeePass.Plugins;
using KeePassLib.Utility;


namespace VeraCryptAutoDismountGT
{
    public partial class FormSettings : Form
    {
        /// <summary>
        /// Plugin Host.
        /// </summary>
        private VeraCryptAutoDismountGTExt plugin = null;
        /// <summary>
        /// KeePass Host.
        /// </summary>
        private IPluginHost m_host = null;

        /// <summary>
        /// Windows Form Constructor.
        /// </summary>
        /// <param name="Plugin">Plugin Host.</param>
        internal FormSettings(VeraCryptAutoDismountGTExt Plugin)
        {
            plugin = Plugin; //Defines variable from argument.
            m_host = plugin.m_host; //Defines variable from argument.
            InitializeComponent(); //Form Initialization.
        }

        /// <summary>
        /// Windows Form Load.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormSettings_Load(object sender, EventArgs e)
        {
            this.Text = VeraCryptAutoDismountGTExt.strSettings + " - " + VeraCryptAutoDismountGTExt.strVeraCryptAutoDismountPlugin; //Set form's name using constants.
            if (Tools.Unix)
            {
                CheckBoxWipeCache.Enabled = false; // Disable wipe on unix flag not implemented.
                RadioButtonDisabled.Text = "Do not dismount.";
                RadioButtonSelected.Text = "Dismount selected VeraCrypt slot. (A=1, B=2 ... Z=26)";
                RadioButtonAll.Text = "Dismount all slots.";
                ofdVeraCryptExecutable.FileName = "veracrypt";
                ofdVeraCryptExecutable.Filter = "VeraCrypt Executable|veracrypt";
            }
            else
            {
                this.label3.Visible = false;
            }
            Working(true); //Sets controls depending on the state of action.
            WorkerLoad.RunWorkerAsync(); //Load Settings in form controls.
        }

        /// <summary>
        /// Windows Form Closing.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormSettings_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (WorkerLoad.IsBusy)
            {
                ButtonCancel.Enabled = false;
                WorkerLoad.CancelAsync();
                e.Cancel = true;
            }
            if (WorkerSave.IsBusy)
            {
                ButtonCancel.Enabled = false;
                WorkerSave.CancelAsync();
                e.Cancel = true;
            }
        }

        private void btnOpenFileDialog_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.txtVeraCryptExecutable.Text))
            {
                try
                {
                    this.ofdVeraCryptExecutable.FileName = Path.GetFileName(this.txtVeraCryptExecutable.Text);
                    this.ofdVeraCryptExecutable.InitialDirectory = Path.GetDirectoryName(this.txtVeraCryptExecutable.Text);
                }
                catch (ArgumentException)
                {
                    MessageService.ShowWarning(string.Format("KeePassVeraCryptMount: Path contains invalid chars -> {0}", this.txtVeraCryptExecutable.Text));
                }
                catch (PathTooLongException)
                {
                    MessageService.ShowWarning(string.Format("KeePassVeraCryptMount: Path is to long -> {0}", this.txtVeraCryptExecutable.Text));
                }
            }

            if (this.ofdVeraCryptExecutable.ShowDialog(this) == DialogResult.OK)
            {
                this.txtVeraCryptExecutable.Text = this.ofdVeraCryptExecutable.FileName;
            }
        }

        private void RadioButtonSelected_CheckedChanged(object sender, EventArgs e)
        {
            ListViewDrives.Enabled = RadioButtonSelected.Checked;
        }

        private void ButtonSave_Click(object sender, EventArgs e)
        {
            Working(true); //Sets controls depending on the state of action.
            WorkerSave.RunWorkerAsync(); //Save Settings.
        }

        private void ButtonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void WorkerLoad_DoWork(object sender, DoWorkEventArgs e)
        {
            var DismountDisabled = m_host.CustomConfig.GetBool(VeraCryptAutoDismountGTExt.setname_bool_DismountDisabled, true);
            var DismountLetters = m_host.CustomConfig.GetString(VeraCryptAutoDismountGTExt.setname_string_DismountLetters, VeraCryptAutoDismountGTExt.setdef_empty_string);
            this.txtVeraCryptExecutable.Text = m_host.CustomConfig.GetString(VeraCryptAutoDismountGTExt.setname_string_VeraCryptPath, VeraCryptAutoDismountGTExt.setdef_empty_string);
            if (!Tools.Unix)
            {
                var Drives = DriveInfo.GetDrives();
                foreach (ListViewItem LVI in ListViewDrives.Items)
                {
                    if (DismountLetters.Contains(LVI.Text[0].ToString()))
                    {
                        LVI.Checked = true;
                    }
                    foreach (var Drive in Drives)
                    {
                        try
                        {
                            if (Drive.Name.StartsWith(LVI.Text))
                            {
                                LVI.Text = LVI.Text + " (" + (Drive.VolumeLabel != "" ? "\"" + Drive.VolumeLabel + "\"" : "null") + ", " + FormatBytes(Drive.TotalSize) + ")";
                            }
                        }
                        catch (Exception)
                        {

                        }
                    }
                }
            }
            var DismountALL = m_host.CustomConfig.GetBool(VeraCryptAutoDismountGTExt.setname_bool_DismountAll, false);
            var WipeCache = m_host.CustomConfig.GetBool(VeraCryptAutoDismountGTExt.setname_bool_WipeCache, false);
            int Result = 0;
            if (DismountDisabled) Result+=8;
            if (!DismountDisabled && !DismountALL) Result+=4;
            if (DismountALL) Result+=2;
            if (WipeCache) Result+=1;
            e.Result = Result;
        }

        private void WorkerLoad_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {

        }

        private void WorkerLoad_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            int Result = (int)e.Result;
            if (Result >= 8)
            {
                RadioButtonDisabled.Checked = true;
                Result-=8;
            }
            if (Result >= 4)
            {
                RadioButtonSelected.Checked = true;
                Result-=4;
            }
            if (Result >= 2)
            {
                RadioButtonAll.Checked = true;
                Result-=2;
            }
            if (Result >= 1)
            {
                CheckBoxWipeCache.Checked = true;
            }
            Working(false); //Sets controls depending on the state of action.
        }

        private void WorkerSave_DoWork(object sender, DoWorkEventArgs e)
        {
            string DismountLetters = "";
            foreach (ListViewItem LVI in ListViewDrives.Items)
            {
                if (LVI.Checked)
                {
                    DismountLetters = DismountLetters + LVI.Text[0];
                }
            }
            m_host.CustomConfig.SetBool(VeraCryptAutoDismountGTExt.setname_bool_DismountDisabled, RadioButtonDisabled.Checked);
            m_host.CustomConfig.SetString(VeraCryptAutoDismountGTExt.setname_string_DismountLetters, DismountLetters);
            m_host.CustomConfig.SetString(VeraCryptAutoDismountGTExt.setname_string_VeraCryptPath, this.txtVeraCryptExecutable.Text);
            m_host.CustomConfig.SetBool(VeraCryptAutoDismountGTExt.setname_bool_DismountAll, RadioButtonAll.Checked);
            m_host.CustomConfig.SetBool(VeraCryptAutoDismountGTExt.setname_bool_WipeCache, CheckBoxWipeCache.Checked);
        }

        private void WorkerSave_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {

        }

        private void WorkerSave_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Working(false); //Sets controls depending on the state of action.
            this.Close();
        }

        /// <summary>
        /// Sets controls depending on the state of action.
        /// </summary>
        /// <param name="Enable"></param>
        private void Working(bool Enable)
        {
            this.UseWaitCursor = Enable;
            RadioButtonDisabled.Enabled = !Enable;
            RadioButtonSelected.Enabled = !Enable;
            ListViewDrives.Enabled = RadioButtonSelected.Checked;
            RadioButtonAll.Enabled = !Enable;
            ButtonSave.Enabled = !Enable;
        }

        /// <summary>
        /// Converts the filesize of a file or drive from byte to best displayable unit.
        /// </summary>
        /// <param name="bytes">Filesize in Bytes</param>
        /// <returns></returns>
        private static string FormatBytes(long bytes)
        {
            string[] Suffix = { "B", "KB", "MB", "GB", "TB" };
            int i = 0;
            double dblSByte = bytes;
            if (bytes > 1024)
            {
                for (i = 0; (bytes/1024) > 0; i++, bytes /= 1024)
                {
                    dblSByte = bytes/1024.0;
                }
            }
            return String.Format("{0:0.##}{1}", dblSByte, Suffix[i]);
        }

        private void txtVeraCryptExecutable_TextChanged(object sender, EventArgs e)
        {
            var executableOk = VeraCryptInfo.ExecutableExists(this.txtVeraCryptExecutable.Text);

            this.txtVeraCryptExecutable.BackColor = executableOk
                ? SystemColors.Window
                : Color.Coral;

            this.ButtonSave.Enabled = executableOk;
        }

        private void btnRegistryResolve_Click(object sender, EventArgs e)
        {
            this.txtVeraCryptExecutable.Text = VeraCryptInfo.ResolveExecutableFromRegistry();
        }

    }
}
