﻿/*
  Plugin for KeePass Password Safe - The Open-Source Password Manager
  VeraCryptAutoDismount
  Copyright (C) 2015-2017 Frédéric Bourqui

  fork of:
  TrueCryptAutoDismountGT

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

namespace VeraCryptAutoDismountGT
{

    using System;
    using System.Drawing;
    using System.Diagnostics;
    using System.Windows.Forms;

    using KeePass.Plugins;
    using KeePass.UI;
    using KeePassLib.Utility;
    using KeePass.Forms;

    /// <summary>
    /// Main Plugin Class
    /// </summary>
    internal sealed class VeraCryptAutoDismountGTExt : Plugin
    {
        /// <summary>
        /// Plugin host Global Reference for access to KeePass functions.
        /// </summary>
        internal IPluginHost m_host = null;

        /// <summary>
        /// Constants (plugin display texts).
        /// </summary>
        internal const string strVeraCryptAutoDismountPlugin = "VeraCrypt AutoDismount Plugin";
        internal const string strSettings = "Settings";

        /// <summary>
        /// Constants (setting names).
        /// </summary>
        internal const string setname_bool_DismountDisabled = "veracrypt.dismount.disabled";
        internal const string setname_string_DismountLetters = "veracrypt.dismount.letters";
        internal const string setname_string_VeraCryptPath = "veracrypt.exe.location";
        internal const string setname_bool_DismountAll = "veracrypt.dismount.all";
        internal const string setname_bool_WipeCache = "veracrypt.dismount.wipe.cache";

        /// <summary>
        /// Constants (default settings values).
        /// </summary>
        internal const string setdef_empty_string = "";

        /// <summary>
        /// Tools Menu Tray Totp Plugin.
        /// </summary>
        private ToolStripMenuItem toMenuVeraCryptAutoDismount = null;

        /// <summary>
        /// Initialization of the plugin, adding menus, handlers and forms.
        /// </summary>
        /// <param name="host">Plugin host for access to KeePass functions.</param>
        /// <returns>Successful loading of the plugin, if not the plugin is removed.</returns>
        public override bool Initialize(IPluginHost host)
        {
            //Internalise Host Handle.
            if (host == null) return false;
            m_host = host;

            //Register form events.
            m_host.MainWindow.FileClosed += MainWindow_FileClosed;
            
            //Tools Menus.
            toMenuVeraCryptAutoDismount = new ToolStripMenuItem(strVeraCryptAutoDismountPlugin);
            toMenuVeraCryptAutoDismount.Image = Properties.Resources.Png_VeraCrypt_Denied;
            toMenuVeraCryptAutoDismount.Click += OnMenuSettingsClick;
            m_host.MainWindow.ToolsMenu.DropDownItems.Add(toMenuVeraCryptAutoDismount);

            return true;
        }

        /// <summary>
        /// Occurs when KeePass is locked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainWindow_FileClosed(object sender, FileClosedEventArgs e)
        {
            if (!m_host.MainWindow.IsAtLeastOneFileOpen())
            {
                string OptionsWipe = "/q /s /w";
                string OptionsUnmountAll = "/q /s /f /d";
                string OptionsUnmount = OptionsUnmountAll + " ";
                if (Tools.Unix)
                {
                    // -w flag doesn't exist on Unix
                    OptionsUnmountAll = "-f -d";
                    OptionsUnmount = OptionsUnmountAll + " --slot ";
                }

                try
                {
                    var VeraCryptLocation = m_host.CustomConfig.GetString(VeraCryptAutoDismountGTExt.setname_string_VeraCryptPath, VeraCryptAutoDismountGTExt.setdef_empty_string);

                    if (!m_host.CustomConfig.GetBool(setname_bool_DismountDisabled, true))
                    {
                        if (!m_host.CustomConfig.GetBool(setname_bool_DismountAll, false))
                        {
                            string DriveSlot;
                            foreach (char DriveLetter in m_host.CustomConfig.GetString(setname_string_DismountLetters, setdef_empty_string))
                            {
                                if (Tools.Unix) DriveSlot = Tools.CapLetterToSlot(DriveLetter);
                                else DriveSlot = DriveLetter.ToString();
                                Process.Start(VeraCryptLocation, OptionsUnmount + DriveSlot);
                            }
                        }
                        else if (m_host.CustomConfig.GetBool(setname_bool_DismountAll, false))
                        {
                            Process.Start(VeraCryptLocation, OptionsUnmountAll);
                        }
                    }
                    if (m_host.CustomConfig.GetBool(setname_bool_WipeCache, false) & !Tools.Unix)
                    {
                        Process.Start(VeraCryptLocation, OptionsWipe);
                    }
                }
                catch (Exception)
                {
                    MessageService.ShowWarning("An error has occured! Please make sure VeraCrypt is defined in Menu Tools - VeraCrypt AutoDismount Plugin.");
                }
            }
        }

        /// <summary>
        /// Tools Menu VeraCrypt AutoDismount Settings Click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnMenuSettingsClick(object sender, EventArgs e)
        {
            var FormSettings = new FormSettings(this);
            UIUtil.ShowDialogAndDestroy(FormSettings);
        }

        /// <summary>
        /// Removes all ressources such as menus, handles and forms from KeePass.
        /// </summary>
        public override void Terminate()
        {
            //Unregister internal events.
            m_host.MainWindow.FileClosed -= MainWindow_FileClosed;

            //Remove Tools Menus.
            m_host.MainWindow.ToolsMenu.DropDownItems.Remove(toMenuVeraCryptAutoDismount);
            toMenuVeraCryptAutoDismount.Dispose();
        }

        /// <summary>
        /// Returns the image of the plugin to display in the KeePass plugin list.
        /// </summary>
        public override Image SmallIcon
        {
            get
            {
                return Properties.Resources.Png_VeraCrypt_Denied;
            }
        }

        /// <summary>
        /// Returns update URL for KeepAss automatic update check. (file must be UTF-8 without BOM (support for BOM fron KP 2.21))
        /// </summary>
        public override string UpdateUrl
        {
            get { return "https://bitbucket.org/fbourqui/keepassveracryptmount/raw/master/version_manifest.txt"; }

        }
    }
}
