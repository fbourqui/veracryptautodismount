﻿/*
  Plugin for KeePass Password Safe - The Open-Source Password Manager
  VeraCryptAutoDismountGT
  Copyright (C) 2015-2017 Frédéric Bourqui

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

namespace VeraCryptAutoDismountGT
{
    using System;

    public static class Tools
    {
        private static bool IsUnix()
        {
            int p = (int)Environment.OSVersion.Platform;
            return (p == 4) || (p == 6) || (p == 128);
        }

        public static bool Unix = IsUnix();

        public static String CapLetterToSlot(char Letter)
        {
            return (Convert.ToInt32(Letter) - 64).ToString();
        }
    }
}
