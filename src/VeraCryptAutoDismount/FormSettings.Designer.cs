﻿namespace VeraCryptAutoDismountGT
{
    partial class FormSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.ListViewItem listViewItem1 = new System.Windows.Forms.ListViewItem("A");
            System.Windows.Forms.ListViewItem listViewItem2 = new System.Windows.Forms.ListViewItem("B");
            System.Windows.Forms.ListViewItem listViewItem3 = new System.Windows.Forms.ListViewItem("C");
            System.Windows.Forms.ListViewItem listViewItem4 = new System.Windows.Forms.ListViewItem("D");
            System.Windows.Forms.ListViewItem listViewItem5 = new System.Windows.Forms.ListViewItem("E");
            System.Windows.Forms.ListViewItem listViewItem6 = new System.Windows.Forms.ListViewItem("F");
            System.Windows.Forms.ListViewItem listViewItem7 = new System.Windows.Forms.ListViewItem("G");
            System.Windows.Forms.ListViewItem listViewItem8 = new System.Windows.Forms.ListViewItem("H");
            System.Windows.Forms.ListViewItem listViewItem9 = new System.Windows.Forms.ListViewItem("I");
            System.Windows.Forms.ListViewItem listViewItem10 = new System.Windows.Forms.ListViewItem("J");
            System.Windows.Forms.ListViewItem listViewItem11 = new System.Windows.Forms.ListViewItem("K");
            System.Windows.Forms.ListViewItem listViewItem12 = new System.Windows.Forms.ListViewItem("L");
            System.Windows.Forms.ListViewItem listViewItem13 = new System.Windows.Forms.ListViewItem("M");
            System.Windows.Forms.ListViewItem listViewItem14 = new System.Windows.Forms.ListViewItem("N");
            System.Windows.Forms.ListViewItem listViewItem15 = new System.Windows.Forms.ListViewItem("O");
            System.Windows.Forms.ListViewItem listViewItem16 = new System.Windows.Forms.ListViewItem("P");
            System.Windows.Forms.ListViewItem listViewItem17 = new System.Windows.Forms.ListViewItem("Q");
            System.Windows.Forms.ListViewItem listViewItem18 = new System.Windows.Forms.ListViewItem("R");
            System.Windows.Forms.ListViewItem listViewItem19 = new System.Windows.Forms.ListViewItem("S");
            System.Windows.Forms.ListViewItem listViewItem20 = new System.Windows.Forms.ListViewItem("T");
            System.Windows.Forms.ListViewItem listViewItem21 = new System.Windows.Forms.ListViewItem("U");
            System.Windows.Forms.ListViewItem listViewItem22 = new System.Windows.Forms.ListViewItem("V");
            System.Windows.Forms.ListViewItem listViewItem23 = new System.Windows.Forms.ListViewItem("W");
            System.Windows.Forms.ListViewItem listViewItem24 = new System.Windows.Forms.ListViewItem("X");
            System.Windows.Forms.ListViewItem listViewItem25 = new System.Windows.Forms.ListViewItem("Y");
            System.Windows.Forms.ListViewItem listViewItem26 = new System.Windows.Forms.ListViewItem("Z");
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormSettings));
            this.WorkerLoad = new System.ComponentModel.BackgroundWorker();
            this.WorkerSave = new System.ComponentModel.BackgroundWorker();
            this.ListViewDrives = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.RadioButtonSelected = new System.Windows.Forms.RadioButton();
            this.RadioButtonAll = new System.Windows.Forms.RadioButton();
            this.LabelSystemFavorites = new System.Windows.Forms.Label();
            this.RadioButtonDisabled = new System.Windows.Forms.RadioButton();
            this.ButtonSave = new System.Windows.Forms.Button();
            this.ButtonCancel = new System.Windows.Forms.Button();
            this.CheckBoxWipeCache = new System.Windows.Forms.CheckBox();
            this.HelpProviderSettings = new System.Windows.Forms.HelpProvider();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtVeraCryptExecutable = new System.Windows.Forms.TextBox();
            this.lblSeparator = new System.Windows.Forms.Label();
            this.btnRegistryResolve = new System.Windows.Forms.Button();
            this.btnOpenFileDialog = new System.Windows.Forms.Button();
            this.ofdVeraCryptExecutable = new System.Windows.Forms.OpenFileDialog();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // WorkerLoad
            // 
            this.WorkerLoad.WorkerReportsProgress = true;
            this.WorkerLoad.WorkerSupportsCancellation = true;
            this.WorkerLoad.DoWork += new System.ComponentModel.DoWorkEventHandler(this.WorkerLoad_DoWork);
            this.WorkerLoad.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.WorkerLoad_ProgressChanged);
            this.WorkerLoad.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.WorkerLoad_RunWorkerCompleted);
            // 
            // WorkerSave
            // 
            this.WorkerSave.WorkerReportsProgress = true;
            this.WorkerSave.WorkerSupportsCancellation = true;
            this.WorkerSave.DoWork += new System.ComponentModel.DoWorkEventHandler(this.WorkerSave_DoWork);
            this.WorkerSave.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.WorkerSave_ProgressChanged);
            this.WorkerSave.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.WorkerSave_RunWorkerCompleted);
            // 
            // ListViewDrives
            // 
            this.ListViewDrives.BackColor = System.Drawing.SystemColors.Window;
            this.ListViewDrives.CheckBoxes = true;
            this.ListViewDrives.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
            this.ListViewDrives.Enabled = false;
            this.ListViewDrives.FullRowSelect = true;
            this.ListViewDrives.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            listViewItem1.StateImageIndex = 0;
            listViewItem2.StateImageIndex = 0;
            listViewItem3.StateImageIndex = 0;
            listViewItem4.StateImageIndex = 0;
            listViewItem5.StateImageIndex = 0;
            listViewItem6.StateImageIndex = 0;
            listViewItem7.StateImageIndex = 0;
            listViewItem8.StateImageIndex = 0;
            listViewItem9.StateImageIndex = 0;
            listViewItem10.StateImageIndex = 0;
            listViewItem11.StateImageIndex = 0;
            listViewItem12.StateImageIndex = 0;
            listViewItem13.StateImageIndex = 0;
            listViewItem14.StateImageIndex = 0;
            listViewItem15.StateImageIndex = 0;
            listViewItem16.StateImageIndex = 0;
            listViewItem17.StateImageIndex = 0;
            listViewItem18.StateImageIndex = 0;
            listViewItem19.StateImageIndex = 0;
            listViewItem20.StateImageIndex = 0;
            listViewItem21.StateImageIndex = 0;
            listViewItem22.StateImageIndex = 0;
            listViewItem23.StateImageIndex = 0;
            listViewItem24.StateImageIndex = 0;
            listViewItem25.StateImageIndex = 0;
            listViewItem26.StateImageIndex = 0;
            this.ListViewDrives.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem1,
            listViewItem2,
            listViewItem3,
            listViewItem4,
            listViewItem5,
            listViewItem6,
            listViewItem7,
            listViewItem8,
            listViewItem9,
            listViewItem10,
            listViewItem11,
            listViewItem12,
            listViewItem13,
            listViewItem14,
            listViewItem15,
            listViewItem16,
            listViewItem17,
            listViewItem18,
            listViewItem19,
            listViewItem20,
            listViewItem21,
            listViewItem22,
            listViewItem23,
            listViewItem24,
            listViewItem25,
            listViewItem26});
            this.ListViewDrives.Location = new System.Drawing.Point(13, 179);
            this.ListViewDrives.MultiSelect = false;
            this.ListViewDrives.Name = "ListViewDrives";
            this.ListViewDrives.Size = new System.Drawing.Size(359, 129);
            this.ListViewDrives.TabIndex = 2;
            this.ListViewDrives.UseCompatibleStateImageBehavior = false;
            this.ListViewDrives.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Width = 286;
            // 
            // RadioButtonSelected
            // 
            this.RadioButtonSelected.AutoSize = true;
            this.RadioButtonSelected.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.HelpProviderSettings.SetHelpString(this.RadioButtonSelected, "Only the selected drives below will be dismounted by the plugin when KeePass will" +
        " be locked.");
            this.RadioButtonSelected.Location = new System.Drawing.Point(17, 154);
            this.RadioButtonSelected.Name = "RadioButtonSelected";
            this.HelpProviderSettings.SetShowHelp(this.RadioButtonSelected, true);
            this.RadioButtonSelected.Size = new System.Drawing.Size(154, 18);
            this.RadioButtonSelected.TabIndex = 1;
            this.RadioButtonSelected.TabStop = true;
            this.RadioButtonSelected.Text = "Dismount selected drives.";
            this.RadioButtonSelected.UseVisualStyleBackColor = true;
            this.RadioButtonSelected.CheckedChanged += new System.EventHandler(this.RadioButtonSelected_CheckedChanged);
            // 
            // RadioButtonAll
            // 
            this.RadioButtonAll.AutoSize = true;
            this.RadioButtonAll.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.HelpProviderSettings.SetHelpString(this.RadioButtonAll, resources.GetString("RadioButtonAll.HelpString"));
            this.RadioButtonAll.Location = new System.Drawing.Point(17, 318);
            this.RadioButtonAll.Name = "RadioButtonAll";
            this.HelpProviderSettings.SetShowHelp(this.RadioButtonAll, true);
            this.RadioButtonAll.Size = new System.Drawing.Size(124, 18);
            this.RadioButtonAll.TabIndex = 3;
            this.RadioButtonAll.TabStop = true;
            this.RadioButtonAll.Text = "Dismount all drives.";
            this.RadioButtonAll.UseVisualStyleBackColor = true;
            // 
            // LabelSystemFavorites
            // 
            this.LabelSystemFavorites.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.LabelSystemFavorites.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.LabelSystemFavorites.Location = new System.Drawing.Point(13, 343);
            this.LabelSystemFavorites.Margin = new System.Windows.Forms.Padding(0);
            this.LabelSystemFavorites.Name = "LabelSystemFavorites";
            this.LabelSystemFavorites.Size = new System.Drawing.Size(359, 43);
            this.LabelSystemFavorites.TabIndex = 4;
            this.LabelSystemFavorites.Text = "NOTE: Drives mounted as operating system favorites cant\'t be dismounted automatic" +
    "ally if the option in VeraCrypt that allows only administrators to modify such d" +
    "rives is enabled.";
            this.LabelSystemFavorites.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // RadioButtonDisabled
            // 
            this.RadioButtonDisabled.AutoSize = true;
            this.RadioButtonDisabled.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.HelpProviderSettings.SetHelpString(this.RadioButtonDisabled, "Disables the plugin, no drives will be dismounted when KeePass will be locked.");
            this.RadioButtonDisabled.Location = new System.Drawing.Point(17, 130);
            this.RadioButtonDisabled.Name = "RadioButtonDisabled";
            this.HelpProviderSettings.SetShowHelp(this.RadioButtonDisabled, true);
            this.RadioButtonDisabled.Size = new System.Drawing.Size(145, 18);
            this.RadioButtonDisabled.TabIndex = 0;
            this.RadioButtonDisabled.TabStop = true;
            this.RadioButtonDisabled.Text = "Do not dismount drives.";
            this.RadioButtonDisabled.UseVisualStyleBackColor = true;
            // 
            // ButtonSave
            // 
            this.ButtonSave.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.HelpProviderSettings.SetHelpString(this.ButtonSave, "Saves the current settings and closes the window.");
            this.ButtonSave.Location = new System.Drawing.Point(212, 473);
            this.ButtonSave.Name = "ButtonSave";
            this.HelpProviderSettings.SetShowHelp(this.ButtonSave, true);
            this.ButtonSave.Size = new System.Drawing.Size(75, 23);
            this.ButtonSave.TabIndex = 5;
            this.ButtonSave.Text = "&Save";
            this.ButtonSave.UseVisualStyleBackColor = true;
            this.ButtonSave.Click += new System.EventHandler(this.ButtonSave_Click);
            // 
            // ButtonCancel
            // 
            this.ButtonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.ButtonCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.HelpProviderSettings.SetHelpString(this.ButtonCancel, "Closes the window without saving the settings.");
            this.ButtonCancel.Location = new System.Drawing.Point(297, 473);
            this.ButtonCancel.Name = "ButtonCancel";
            this.HelpProviderSettings.SetShowHelp(this.ButtonCancel, true);
            this.ButtonCancel.Size = new System.Drawing.Size(75, 23);
            this.ButtonCancel.TabIndex = 6;
            this.ButtonCancel.Text = "&Cancel";
            this.ButtonCancel.UseVisualStyleBackColor = true;
            this.ButtonCancel.Click += new System.EventHandler(this.ButtonCancel_Click);
            // 
            // CheckBoxWipeCache
            // 
            this.CheckBoxWipeCache.AutoSize = true;
            this.CheckBoxWipeCache.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.HelpProviderSettings.SetHelpString(this.CheckBoxWipeCache, "Additionnal option that wipes VeraCrypt cache. Can be enabled even if the dismoun" +
        "t options are not selected.");
            this.CheckBoxWipeCache.Location = new System.Drawing.Point(17, 394);
            this.CheckBoxWipeCache.Name = "CheckBoxWipeCache";
            this.HelpProviderSettings.SetShowHelp(this.CheckBoxWipeCache, true);
            this.CheckBoxWipeCache.Size = new System.Drawing.Size(293, 18);
            this.CheckBoxWipeCache.TabIndex = 7;
            this.CheckBoxWipeCache.Text = "Wipe passwords cached in VeraCrypt\'s driver memory.";
            this.CheckBoxWipeCache.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label1.Location = new System.Drawing.Point(13, 52);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(359, 69);
            this.label1.TabIndex = 8;
            this.label1.Text = resources.GetString("label1.Text");
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(137, 13);
            this.label2.TabIndex = 22;
            this.label2.Text = "VeraCrypt executable path";
            // 
            // txtVeraCryptExecutable
            // 
            this.txtVeraCryptExecutable.BackColor = System.Drawing.Color.Coral;
            this.txtVeraCryptExecutable.Location = new System.Drawing.Point(13, 23);
            this.txtVeraCryptExecutable.Name = "txtVeraCryptExecutable";
            this.txtVeraCryptExecutable.Size = new System.Drawing.Size(274, 21);
            this.txtVeraCryptExecutable.TabIndex = 21;
            this.txtVeraCryptExecutable.TextChanged += new System.EventHandler(this.txtVeraCryptExecutable_TextChanged);
            // 
            // lblSeparator
            // 
            this.lblSeparator.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblSeparator.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSeparator.Location = new System.Drawing.Point(-1, 466);
            this.lblSeparator.Name = "lblSeparator";
            this.lblSeparator.Size = new System.Drawing.Size(386, 2);
            this.lblSeparator.TabIndex = 27;
            // 
            // btnRegistryResolve
            // 
            this.btnRegistryResolve.Image = global::VeraCryptAutoDismountGT.Properties.Resources.B16x16_System_Registry;
            this.btnRegistryResolve.Location = new System.Drawing.Point(337, 21);
            this.btnRegistryResolve.Name = "btnRegistryResolve";
            this.btnRegistryResolve.Size = new System.Drawing.Size(35, 23);
            this.btnRegistryResolve.TabIndex = 24;
            this.btnRegistryResolve.UseVisualStyleBackColor = true;
            this.btnRegistryResolve.Click += new System.EventHandler(this.btnRegistryResolve_Click);
            // 
            // btnOpenFileDialog
            // 
            this.btnOpenFileDialog.Image = global::VeraCryptAutoDismountGT.Properties.Resources.B16x16_Folder_Yellow_Open;
            this.btnOpenFileDialog.Location = new System.Drawing.Point(296, 21);
            this.btnOpenFileDialog.Name = "btnOpenFileDialog";
            this.btnOpenFileDialog.Size = new System.Drawing.Size(35, 23);
            this.btnOpenFileDialog.TabIndex = 23;
            this.btnOpenFileDialog.UseVisualStyleBackColor = true;
            this.btnOpenFileDialog.Click += new System.EventHandler(this.btnOpenFileDialog_Click);
            // 
            // ofdVeraCryptExecutable
            // 
            this.ofdVeraCryptExecutable.FileName = "VeraCrypt.exe";
            this.ofdVeraCryptExecutable.Filter = "VeraCrypt Executable|VeraCrypt.exe";
            this.ofdVeraCryptExecutable.RestoreDirectory = true;
            // 
            // label3
            // 
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label3.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label3.Location = new System.Drawing.Point(13, 415);
            this.label3.Margin = new System.Windows.Forms.Padding(0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(359, 43);
            this.label3.TabIndex = 28;
            this.label3.Text = "NOTE: wipe flag is not available on Unix. Can be configured in VeraCrypt\r\nSetting" +
    "s Menu - Preferences... - Security Tab - Password Cache - \r\nWipe after VeraCrypt" +
    " window has been closed.";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FormSettings
            // 
            this.AcceptButton = this.ButtonSave;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.ButtonCancel;
            this.ClientSize = new System.Drawing.Size(384, 501);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblSeparator);
            this.Controls.Add(this.btnRegistryResolve);
            this.Controls.Add(this.btnOpenFileDialog);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtVeraCryptExecutable);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CheckBoxWipeCache);
            this.Controls.Add(this.ButtonCancel);
            this.Controls.Add(this.ButtonSave);
            this.Controls.Add(this.RadioButtonDisabled);
            this.Controls.Add(this.LabelSystemFavorites);
            this.Controls.Add(this.RadioButtonAll);
            this.Controls.Add(this.RadioButtonSelected);
            this.Controls.Add(this.ListViewDrives);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormSettings";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "_settings_";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormSettings_FormClosing);
            this.Load += new System.EventHandler(this.FormSettings_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.ComponentModel.BackgroundWorker WorkerLoad;
        private System.ComponentModel.BackgroundWorker WorkerSave;
        private System.Windows.Forms.ListView ListViewDrives;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.RadioButton RadioButtonSelected;
        private System.Windows.Forms.RadioButton RadioButtonAll;
        private System.Windows.Forms.Label LabelSystemFavorites;
        private System.Windows.Forms.RadioButton RadioButtonDisabled;
        private System.Windows.Forms.Button ButtonSave;
        private System.Windows.Forms.Button ButtonCancel;
        private System.Windows.Forms.CheckBox CheckBoxWipeCache;
        private System.Windows.Forms.HelpProvider HelpProviderSettings;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtVeraCryptExecutable;
        private System.Windows.Forms.Button btnRegistryResolve;
        private System.Windows.Forms.Button btnOpenFileDialog;
        private System.Windows.Forms.Label lblSeparator;
        private System.Windows.Forms.OpenFileDialog ofdVeraCryptExecutable;
        private System.Windows.Forms.Label label3;
    }
}